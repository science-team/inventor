#! /bin/sh -e

fontpath=/usr/share/inventor/fonts
type1=/usr/share/fonts/X11/Type1
urw=/usr/share/fonts/type1/urw-base35

mkdir -p $fontpath
cd $fontpath

ln -s -f $type1/c0419bt_.pfb  Courier-Regular
ln -s -f $type1/c0582bt_.pfb  Courier-Italic
ln -s -f $type1/c0583bt_.pfb  Courier-Bold
ln -s -f $type1/c0611bt_.pfb  Courier-BoldItalic
ln -s -f $urw/C059-Roman.t1  Century-Schoolbook-Roman
ln -s -f $urw/C059-Bold.t1  Century-Schoolbook-Bold
ln -s -f $urw/C059-Italic.t1  Century-Schoolbook-Italic
ln -s -f $urw/C059-BdIta.t1  Century-Schoolbook-BoldItalic


for i in $type1/*.pfa; do
    test -f $i || continue
    out=.`egrep ^/FontName $i | cut -d' ' -f2`
    #echo "Linking $i to $out"
    if [ ! -e $out ]; then
	ln -s -f $i $out
    fi
done

# make sure the fallback font is available
if [ ! -e Utopia-Regular ]; then
    ln -s -f Century-Schoolbook-Roman Utopia-Regular
fi
